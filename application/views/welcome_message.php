<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>New Hire Coding Test</title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.5/datatables.min.css"/>

	<script type="text/javascript" src="<?php echo base_url(); ?>js/jQuery-3.6.0/jquery-3.6.0.js"></script>

	<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.5/datatables.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>js/welcome.js"></script>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
		text-decoration: none;
	}

	a:hover {
		color: #97310e;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
		min-height: 96px;
	}

	p {
		margin: 0 0 10px;
		padding:0;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>
	<div id="body">
		<div class="row">
			<div class="col">
				<h2>Part 2:</h2>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<table id="exampleTable" class="display" style="width:100%">
					<thead>
						<tr>
							<th>Row Id</th>
							<th>Task</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					<?php
					if(count($entries)) {
						foreach ($entries as $entry) {
							?>
							<tr>
								<td><?=$entry->id?></td>
								<td id="row-<?= $entry->id ?>"><?= $entry->text ?></td>
							<td>
								<button class="btn btn-warning edit" rowid="<?= $entry->id ?>">Edit</button>&nbsp;<button
									class="btn btn-danger delete" rowid="<?= $entry->id ?>">Delete
								</button>
							</td>
							</tr>
							<?php
						}
					}
					?>
					</tbody>
				</table>
			</div>
		</div>

		<div class="row">
			<div class="col">
				<?php
						echo form_open('welcome/save_input');
						echo form_label("Input New Row: ", "text", "id='textInputLabel'" );
						$inputArr = array("type" => "text", "name" => "val", "id" => "textInput", "class" => "form_control"); //makes text input
						echo form_input($inputArr);

						$hiddenArr = array("type" => "hidden", "id" => "id", "name" => "id"); //makes hidden id input for updating row
						echo form_input($hiddenArr);

						echo form_submit('btnSubmit', 'Submit',  "class='btn btn-primary'");
						echo form_button('', 'Clear', "class='btn btn-danger' id='clearForm'");
						echo form_close();
				?>&nbsp;

			</div>
	</div>
		</div>
</div>

</body>
</html>
