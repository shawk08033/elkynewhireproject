<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */

	public function __construct() {
		parent::__construct();
		$this->load->helper('url');

		$this->load->model('stepThree_model');
	}

	/**
	 * Event for the delete button
	 * @param $id int id of database row to delete
	 */
	public function delete($id)
	{

		$this->stepThree_model->delete_entry_text($id);
		redirect('welcome/index');
	}

	public function index()
	{
		$data['entries'] = $this->stepThree_model->get_all();
		$data['test'] = "test";

		$this->load->view('welcome_message', $data);
	}

	/**
	 * call when form is submitted determines if entree is in database yet, if not an entry is create else the entry is updated
	 */
	function save_input()
	{
		$id = $this->input->post('id');
		$val = $this->input->post('val');


		if($id != "")
		{
			$this->stepThree_model->save_entry_text($id, $val);
			redirect('welcome/index');

		}
		else
		{
			$this->stepThree_model->insert_entry_text($val);
			redirect('welcome/index');
		}
	}


}
