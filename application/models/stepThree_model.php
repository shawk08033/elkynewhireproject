<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class stepThree_model extends CI_Model
{


	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Return all rows from db
	 * @return mixed
	 */
	function get_all()
	{
		$query = $this->db->get('stepthree');

			return $query->result();

	}

	/**
	 * Saves text to column using id as the primary key
	 * @param $id int primary key
	 * @param $value value of
	 */
	function save_entry_text($id, $value)
	{
		$data = array('text' => $value);

		$where = "id='" . $id . "'";

		$query = $this->db->update_string('stepthree', $data, $where);
		echo $query;

		$this->db->simple_query($query);
	}

	/**
	 * Inserts new row with $value as the value
	 * @param $value string value inserted into row
	 */
	function insert_entry_text($value)
	{
		$data = array('text' => $this->db->escape_str($value));

		$query = $this->db->insert_string('stepthree', $data);

		$this->db->simple_query($query);
	}

	/**
	 * Deletes row with $value as the value with the id pass into the function
	 * @param $id int row id
	 */
	function delete_entry_text($id)
	{
		$query = $this->db->simple_query("DELETE FROM stepthree WHERE id='$id'");

		echo "DELETE FROM stepthree WHERE id='$id'";
	}

}
