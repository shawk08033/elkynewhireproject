$(document).ready(function () {
		$('#exampleTable').DataTable();

	$(".delete").click(function() {
		console.log("index.php/welcome/delete/" + $(this).attr("rowId"));
		window.location.pathname = "index.php/welcome/delete/" + $(this).attr("rowId");
	});

	$(".edit").click(function () {
		var id = $(this).attr("rowid");
		var val = $("#row-" + id).text();
		$("#textInput").val(val);
		$("#id").val(id);
		$("#textInputLabel").text("Update Row " + id + ": ");
	});

	$("#clearForm").click(function () {
		$("#textInput").val("");
		$("#id").val("");
		$("#textInputLabel").text("Input New Row: ");
	})
	});
